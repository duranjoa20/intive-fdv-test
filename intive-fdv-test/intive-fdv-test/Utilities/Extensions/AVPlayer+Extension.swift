//
//  AVPlayer+Extension.swift
//  intive-fdv-test
//
//  Created by Joaquin  on 01/05/2019.
//  Copyright © 2019 Joaquin . All rights reserved.
//

import Foundation
import AVFoundation

extension AVPlayer {
    var isPlaying: Bool {
        return rate != 0 && error == nil
    }
    func stop() {
        self.rate = 0
        self.replaceCurrentItem(with: nil)
    }
}

