//
//  StoryBoard+Extension.swift
//  intive-fdv-test
//
//  Created by Joaquin  on 01/05/2019.
//  Copyright © 2019 Joaquin . All rights reserved.
//

import Foundation
import UIKit

extension UIStoryboard{

    struct intiveTest{

        //MARK: - Load storyboard
        static func mainStoryboard() -> UIStoryboard {
            return UIStoryboard(name: "Main", bundle: nil)
        }


        //MARK: - Main storyboard
        static func instantiateContactDetailsViewController() -> MediaTableViewController {
            let storyboard = UIStoryboard.intiveTest.mainStoryboard()
            return storyboard.instantiateViewController(withIdentifier: "MediaTableViewController") as! MediaTableViewController
        }

    }

}

