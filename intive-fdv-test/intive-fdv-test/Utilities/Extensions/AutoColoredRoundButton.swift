//
//  AutoColoredRoundButton.swift
//  intive-fdv-test
//
//  Created by Joaquin  on 01/05/2019.
//  Copyright © 2019 Joaquin . All rights reserved.
//


import UIKit

class AutoColoredUIButton: UIButton {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.setup()

    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()



    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()

    }

    func setup() {
        self.setTitleColor(.white, for: .normal)
        self.layer.cornerRadius = 0.05 * self.bounds.size.width
        self.clipsToBounds = true
        self.backgroundColor = #colorLiteral(red: 0.4239391387, green: 0.3240071535, blue: 0.7302607894, alpha: 1)

    }
}
