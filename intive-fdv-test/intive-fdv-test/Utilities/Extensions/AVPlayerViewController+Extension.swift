//
//  AVPlayerViewController+Extension.swift
//  intive-fdv-test
//
//  Created by Joaquin  on 01/05/2019.
//  Copyright © 2019 Joaquin . All rights reserved.
//


import Foundation
import UIKit
import AVKit
import AVFoundation

extension AVPlayerViewController {

    override open func viewDidDisappear(_ animated: Bool) {
        (UIApplication.shared.delegate as! AppDelegate).restrictRotation = .portrait
        super.viewDidDisappear(animated)
        self.player?.stop()
    }

    override open func viewWillAppear(_ animated: Bool) {
        (UIApplication.shared.delegate as! AppDelegate).restrictRotation = .all
        super.viewWillAppear(animated)
    }

}
