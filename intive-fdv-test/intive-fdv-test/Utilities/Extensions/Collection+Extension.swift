//
//  Collection+Extension.swift
//  intive-fdv-test
//
//  Created by Joaquin  on 30/04/2019.
//  Copyright © 2019 Joaquin . All rights reserved.
//
import Foundation


extension Collection {

    func at(_ index: Self.Index) -> Element? {
        if startIndex <= index && index < endIndex {
            return self[index]
        } else {
            return nil
        }
    }

}
