//
//  UIAlertController+Extension.swift
//  intive-fdv-test
//
//  Created by Joaquin  on 01/05/2019.
//  Copyright © 2019 Joaquin . All rights reserved.
//

import Foundation
import UIKit

extension UIAlertController {

    public static func showCustomMessageAlert(title: String? = nil, message: String?, presenter: UIViewController, handler: ((UIAlertAction) -> Swift.Void)? = nil) {
        let alert = UIAlertController(title: title , message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default, handler: handler)
        alert.addAction(ok)
        presenter.present(alert, animated: true, completion: nil)
    }

}

