//
//  AVHelper.swift
//  intive-fdv-test
//
//  Created by Joaquin  on 01/05/2019.
//  Copyright © 2019 Joaquin . All rights reserved.
//

import Foundation
import AVFoundation
import AVKit
import UIKit

class AVHelper {

    let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
    let playerViewController = AVPlayerViewController()
    var audioPlayer = AVPlayer()
    var lastUrl: URL?
    static var shared = AVHelper()

    @objc func playerDidFinishPlaying(note: NSNotification) {
        self.navigationController.popViewController(animated: true)
    }



    func playVideo(url: URL) {
        NotificationCenter.default.addObserver(
            self,
            selector:#selector(self.playerDidFinishPlaying(note:)),
            name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
            object: nil)

        let player = AVPlayer(url: url)
        playerViewController.player = player
        playerViewController.showsPlaybackControls = false
        self.navigationController.pushViewController(playerViewController, animated: true)
        self.playerViewController.player!.play()

    }

    func playAudio(url: URL) {
        	
        if audioPlayer.isPlaying, url == lastUrl {
            audioPlayer.stop()
        } else {
            let playerItem = AVPlayerItem(url: url)
            self.audioPlayer = AVPlayer(playerItem:playerItem)
            audioPlayer.play()
        }

        lastUrl = url
    }

    func stopAudio() {
        audioPlayer.stop()
    }
    
}
