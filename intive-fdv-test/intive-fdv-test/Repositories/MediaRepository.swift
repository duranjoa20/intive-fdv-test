//
//  MediaRepository.swift
//  intive-fdv-test
//
//  Created by Joaquin  on 01/05/2019.
//  Copyright © 2019 Joaquin . All rights reserved.
//

import Foundation

struct MediaRepository: Decodable {

    var results: [SearchResults]?
    var status: String?

    static var shared = MediaRepository()

    func getUserPayments(searchTerm: String, mediaType: Media, completition: @escaping ([SearchResults]?) -> Void) {
        Rest.shared.searchMedia(searchTerm: searchTerm, mediaType: mediaType) { (results) in
                completition(results)
        }
    }
}
