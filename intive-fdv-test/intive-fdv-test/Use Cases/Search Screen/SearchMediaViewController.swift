//
//  SearchMediaViewController.swift
//  intive-fdv-test
//
//  Created by Joaquin  on 30/04/2019.
//  Copyright © 2019 Joaquin . All rights reserved.
//

import UIKit
import ImageSlideshow

class SearchMediaViewController:  UIViewController{

    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var sliderShow: ImageSlideshow!
    @IBOutlet weak var backgroundImage: UIImageView!
    var media: Media = .none


    override func viewDidLoad() {
        super.viewDidLoad()
        setupSliderShow()
        backgroundImage.image = UIImage(imageLiteralResourceName: "background")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }


    @IBAction func didTouchBackground(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)

    }

    @IBAction func didTouchSearchMedia(_ sender: UIButton) {
        let imageIndex = sliderShow.currentPage
        self.media = Media.getMedia(from: imageIndex)
        MediaRepository.shared.getUserPayments(searchTerm: searchTextField.text!, mediaType: self.media) { (results) in
            guard let results = results, results.count > 0 else {
                UIAlertController.showCustomMessageAlert(message: "There was a problem with your search. Why don't you try a different one?", presenter: self)
                return
            }
            let vc = UIStoryboard.intiveTest.instantiateContactDetailsViewController()
            vc.mediaArray = results
            vc.mediaType = self.media
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

    fileprivate func setupSliderShow() {
        sliderShow.setImageInputs([
            ImageSource(image: UIImage(imageLiteralResourceName: "movies")),
            ImageSource(image: UIImage(imageLiteralResourceName: "tvShow")),
            ImageSource(image: UIImage(imageLiteralResourceName: "music"))
            ])
        sliderShow.layer.cornerRadius = 20
    }

}
