//
//  MediaTableViewCell.swift
//  intive-fdv-test
//
//  Created by Joaquin  on 01/05/2019.
//  Copyright © 2019 Joaquin . All rights reserved.
//

import UIKit
import SDWebImage

class MediaTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var trackNameLabel: UILabel!
    @IBOutlet weak var mediaImage: UIImageView!
    @IBOutlet weak var longDescriptionLabel: UILabel!


    func setUpWith(searchResult: SearchResults) {
        nameLabel.text = searchResult.artistName
        trackNameLabel.text = searchResult.trackName
        longDescriptionLabel.text = searchResult.longDescription
        guard let imagePath = searchResult.artworkUrl100, let url = URL(string: imagePath) else { return }
        mediaImage.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "User"))

    }

}
