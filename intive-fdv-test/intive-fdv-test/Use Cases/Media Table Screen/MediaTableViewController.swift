//
//  MediaTableViewController.swift
//  intive-fdv-test
//
//  Created by Joaquin  on 01/05/2019.
//  Copyright © 2019 Joaquin . All rights reserved.
//

import Foundation
import UIKit
import AVKit
import AVFoundation

class MediaTableViewController: UIViewController {


    var mediaArray: [SearchResults] = []
    @IBOutlet weak var mediaTableView: UITableView!
    let playerViewController = AVPlayerViewController()
    var mediaType = Media.none

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        setupTableView()
    }

    fileprivate func setupTableView() {
        self.mediaTableView.delegate = self
        self.mediaTableView.dataSource = self
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        AVHelper.shared.stopAudio()
    }


}


extension MediaTableViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mediaArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MediaTableViewCell", for: indexPath) as! MediaTableViewCell

        guard let searchResults = mediaArray.at(indexPath.row) else { return cell }
        cell.setUpWith(searchResult: searchResults)

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = indexPath.row
        guard let mediaPath = mediaArray[index].previewUrl, let fileURL = URL(string: mediaPath) else { return }
        switch mediaType {
        case .movie, .tvShow:
            AVHelper.shared.playVideo(url: fileURL)
        case .music:
            AVHelper.shared.playAudio(url: fileURL)
        default:
            UIAlertController.showCustomMessageAlert(message: "The media type is still not supported", presenter: self)
        }

    }
}

extension MediaTableViewController: UITableViewDataSource {

}
