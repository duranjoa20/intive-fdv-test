//
//  SearchResults.swift
//  intive-fdv-test
//
//  Created by Joaquin  on 01/05/2019.
//  Copyright © 2019 Joaquin . All rights reserved.
//

import Foundation

struct SearchResults: Decodable {
    let artistName: String?
    let trackName: String?
    let artworkUrl100: String?
    let longDescription: String?
    let previewUrl: String?

}
