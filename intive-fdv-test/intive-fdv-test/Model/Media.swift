//
//  Media.swift
//  intive-fdv-test
//
//  Created by Joaquin  on 01/05/2019.
//  Copyright © 2019 Joaquin . All rights reserved.
//

import Foundation
enum Media: String, Decodable {
    case music = "music"
    case tvShow = "tvShow"
    case movie = "movie"
    case none = "all"
    
    static func getMediaNames() -> [String]{
        return ["Music", "Tv Show", "Movie"]
    }
    static func getMedia(from: Int) -> Media{
        switch from {
        case 0:
            return .movie
        case 1:
            return .tvShow
        case 2:
            return .music
        default:
            return .none
        }
    }
}
