//
//  InfoResponse.swift
//  intive-fdv-test
//
//  Created by Joaquin  on 01/05/2019.
//  Copyright © 2019 Joaquin . All rights reserved.
//

import Foundation

struct InfoResponse<T: Decodable>: Decodable {
    var results: [T]
    var resultCount: Int?

}
