//
//  RestServicePaths.swift
//  intive-fdv-test
//
//  Created by Joaquin  on 30/04/2019.
//  Copyright © 2019 Joaquin . All rights reserved.
//


import Foundation

public struct RestServicePath {


    #if DEBUG
        static let apiHost = "https://itunes.apple.com/"  //DEV
    #else
        static let apiHost = "https://itunes.apple.com/" //PROD
    #endif

    static let searchMedia = "\(apiHost)search?"

}
