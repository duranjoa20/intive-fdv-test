//
//  SearchMediaAPI.swift
//  intive-fdv-test
//
//  Created by Joaquin  on 01/05/2019.
//  Copyright © 2019 Joaquin . All rights reserved.
//

import Foundation
import Alamofire

extension Rest {

    public typealias SearchMedia = (_ infoResponse: [SearchResults]?) -> Void

    public func searchMedia(searchTerm: String, mediaType: Media, completition: @escaping SearchMedia) {

        let params = [
            "media": mediaType.rawValue,
            "term": searchTerm
        ]

        self.sessionManager.request(RestServicePath.searchMedia, parameters: params, headers: self.defaultHeaders())
            .validate()
            .responseJSON { (dataResponse) in
                guard let data = dataResponse.data else {
                    completition (nil)
                    return
                }
                do {
                    let decodedData = try JSONDecoder().decode(InfoResponse<SearchResults>.self, from: data).results
                    completition(decodedData)
                } catch let error {
                    debugPrint(error)
                    completition(nil)

                }
        }
    }
}
