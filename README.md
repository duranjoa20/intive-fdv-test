# Intive-FDV-Test

This is a coding test for Intive-FDV

Introduction:
For this test, I applied  2 different patterns, which I will explain later, that included singleton and MVC. I limited myself to cover the whole extent of the test, which means I left out some key features and concepts that were beyond the scope of the project.

Design pattern: Singleton
Singletons have proven very common among the Swift community. Its usefulness falls to the shared functionality that they provide throughout the whole project without needing to be stored by the classes that use them.
Examples of singletons can be found in the Rest and AVPlayer classes. There are many arguments against the usage of this pattern, which for the purpose of this test will not be taking into consideration.

Architectural pattern: MVC
MVC is one of the most common patterns in low scale applications. Its simplicity proves very handy when developing this kind of application since it provides a fast-paced, well-organized way of communicating between model, views, and controllers

Things I left out:
The Rest class seems very empty. The way it was structured was to support future proof changes in case this were to grow. I would like the class to have things like token validation and different authentications but since they were not required they were left out

To sum up :
This project as-is should not be taken as an example for large scale applications. MVC can be painfully hard to maintain if the controllers grow in functionality. Singletons for the Rest class can be kept but it can be an obstacle for things like the AVPlayer. An example of this could be not supporting multiple media streams at once (Since only one instance can take place at a given time). 
Since this was out of the project scope they were kept. But were the application to grow a new look into the design and architecture could prove useful in the long run
